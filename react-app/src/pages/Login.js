import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(props) {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState('');

  function authenticate(e) {
    e.preventDefault();

    fetch(`${ process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: localStorage.getItem('token')
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            title: 'Login Successful',
            icon: 'success',
            text: "Welcome to Bud's Place Games!"
          });
        } else {
          Swal.fire({
            title: 'Authentication Failed',
            icon: 'error',
            text: 'Check your login details and try again.'
          });
        }
      });

    localStorage.setItem('email', email);

    setEmail('');
    setPassword('');

    console.log(`${email} has been verified! Welcome back!`);
  }

  const retrieveUserDetails = (token) => {
   return fetch(`${ process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
        Authorization: localStorage.getItem('token')
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

    setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        });
    };

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  if (user.id !== null) {
    return user.isAdmin ? <Navigate to="/admin" /> : <Navigate to="/products" />;
  }
  return (
    <Container className="d-flex justify-content-center align-items-center" style={{ minHeight: '100vh' }}>
      <Form onSubmit={e => authenticate(e)} style={{ maxWidth: '400px', width: '100%', border: '1px solid #ccc', padding: '20px' }}>
      <h1 className="text-center mb-4">Login</h1>
        <Form.Group controlId="userEmail" style={{ marginBottom: '1rem' }}>
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={e => setEmail(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="password" style={{ marginBottom: '1rem' }}> 
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={e => setPassword(e.target.value)}
            required
          />
        </Form.Group>

        {isActive ? (
          <Button variant="primary" type="submit" className="mt-3" id="submitBtn">
            Submit
          </Button>
        ) : (
          <Button variant="primary" type="submit" className="mt-3" id="submitBtn" disabled>
            Submit
          </Button>
        )}
      </Form>
    </Container>
  );
}
