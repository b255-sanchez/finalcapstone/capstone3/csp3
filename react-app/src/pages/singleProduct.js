import { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';

export default function SingleProduct() {
  const [product, setProduct] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const { id } = useParams();

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const res = await fetch(`${ process.env.REACT_APP_API_URL}/products/${id}`);
        if (!res.ok) {
          throw new Error('Failed to fetch product');
        }
        const data = await res.json();
        setProduct(data);
        setLoading(false);
      } catch (err) {
        console.error(err);
        setError(true);
        setLoading(false);
      }
    };
    fetchProduct();
  }, [id]);

  const handleQuantityChange = (event) => {
    setQuantity(Number(event.target.value));
  };

  const handleCheckout = () => {
    // Implement checkout functionality here
    console.log(`Checking out ${quantity} units of ${product.name}`);
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error occurred while loading product details.</div>;
  }

  return (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <div style={{ border: '1px solid black', padding: '20px' }}>
      <h1>{product.name}</h1>
      <p>{product.description}</p>
      <p>Price: PhP {product.price}</p>
      <img src={product.profilePictureURL} style={{ maxWidth: '100%' }} />
      <br />
      <input className="mt-3"
        type="number"
        id="quantity"
        name="quantity"
        min="1"
        value={quantity}
        onChange={handleQuantityChange}
      />
      <br />
      <button className="btn btn-success mt-3" onClick={handleCheckout}>Check Out</button>
      <br /><br />
      <Link to="/products">Back to Products</Link>

    </div>
    </div>
  );
}


