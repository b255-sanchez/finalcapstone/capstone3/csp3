import { Link } from 'react-router-dom';

export default function NotFound() {
  return (
    <div>
      <h1>Page Not Found</h1>
      <p>The requested page does not exist.</p>
      <p>Return to the <Link to="/">home page</Link></p>
    </div>
  );
}