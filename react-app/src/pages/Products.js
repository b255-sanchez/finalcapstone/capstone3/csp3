import { Fragment, useEffect, useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import ProductCard from '../components/Product';

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${ process.env.REACT_APP_API_URL}/products/all`)
      .then(res => res.json())
      .then(data => {
        setProducts(data);
      });
  }, []);

  return (
    <Fragment>
      <Row>
        {products.map(product => (
          <Col sm={4} key={product._id}>
            <ProductCard productProp={product} />
          </Col>
        ))}
      </Row>
    </Fragment>
  );
}