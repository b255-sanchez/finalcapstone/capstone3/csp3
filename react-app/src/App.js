import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes} from 'react-router-dom';
import React from 'react';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights'
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Admin from './components/Admin';
import Logout from './pages/logout';
import NotFound from './pages/NotFound';
import { useState, useEffect } from 'react';
import SingleProduct from './pages/singleProduct';

import './App.css';
import { UserProvider } from './UserContext';


function App() {
  // State hook for the user state that's defined here for a global scope
  // Initialized as an object with properties from the local storage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on logout
  const unsetUser = () =>{
    localStorage.clear()
  }

  // Used to check if the user information is properly stored upon login
  useEffect(() => {
    console.log(user);
    console.log(localStorage)
  }, [user])


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <Container fluid>
              <AppNavBar />
              <Routes>
                  <Route path="/" element={<Home/>} exact="true"/>
                  <Route path="/products" element={<Products />} exact="true"/>
                  <Route path="/admin" element={<Admin />} exact="true"/>
                  <Route path="/productView/:productId" element={<ProductView exact="true"/>} />
                  <Route path="/register" element={<Register />} exact="true"/>
                  <Route path="/login" element={<Login />} exact="true"/>
                  <Route path="/logout" element={<Logout />} exact="true"/>
                  <Route path="/products/:id" element={<SingleProduct />} exact="true"/>
                  <Route path="*" element={<NotFound />} exact="true"/>
              </Routes>
          </Container>
        </Router>
      </UserProvider>
  );
}

export default App;
