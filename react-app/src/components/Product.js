import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import SingleProduct from '../pages/singleProduct';

export default function ProductCard({ productProp }) {
  const { name, description, price, _id } = productProp;

  return (
    <Card style={{ marginBottom: "20px" }}>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>
        <Link className="btn btn-primary" to={{
          pathname: `/products/${_id}`,
          state: { product: productProp }
        }}>Details</Link>
      </Card.Body>
    </Card>
  );
}
ProductCard.propTypes = {
  // The shape method is used to check if a prop object conforms to a specific shape
  productProp: PropTypes.shape({
    // Define the properties and their expected types
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
