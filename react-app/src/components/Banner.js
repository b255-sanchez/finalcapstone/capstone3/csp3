// import Button from 'react-bootstrap/Button';
// // Bootstrap grid system components
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';
import { Button, Row, Col, Carousel } from 'react-bootstrap';

export default function Banner({ title, subtitle, buttonLabel }) {
  return (
    <div>
    
      <Row>
          <Col className="mx-auto">
              <Carousel className="w-90">
                  <Carousel.Item>
                      <img
                          className="d-block w-100"
                          src="https://e0.pxfuel.com/wallpapers/215/319/desktop-wallpaper-my-resident-evil-4-remake-residentevil.jpg"
                      />
                  </Carousel.Item>
                  <Carousel.Item>
                      <img
                          className="d-block w-100"
                          src="https://images7.alphacoders.com/130/1303357.jpg"
                      />
                  </Carousel.Item>
                  <Carousel.Item>
                      <img
                          className="d-block w-100"
                          src="https://meetyourmakergame.com/static/e72ffe11e5b67393408e9e555ade9b52/1fe76/MYM_Blogpost_Main_Image_P_Splus_89ee29bbbd.jpg"
                      />
                  </Carousel.Item>
              </Carousel>
          </Col>
      </Row>
      <Row>
          <Col className="p-5 banner-col">
              <h1>Adding fun to your life</h1>
              <p>Lets you play better!</p>
              <Button variant="primary">Order now!</Button>
          </Col>
      </Row>
    </div>
  );
}
