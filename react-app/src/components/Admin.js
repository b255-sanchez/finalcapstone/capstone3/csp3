import React, { useState, useEffect } from 'react';
import { Container, Table, Button, Modal, Form } from 'react-bootstrap';


const AdminDashboard = () => {
  const [products, setProducts] = useState([]);
  const [showAddModal, setShowAddModal] = useState(false);
  const [editProduct, setEditProduct] = useState(null);
  const [showEditModal, setShowEditModal] = useState(false);

  const [newProduct, setNewProduct] = useState({
    name: '',
    description: '',
    price: '',
    isAvailable: true
  });


  const handleOpenEditModal = (product) => {
  setEditProduct(product);
  setShowEditModal(true);
};

  const handleInputChange = (event) => {
  const { name, value } = event.target;
  setNewProduct((prevProduct) => ({
    ...prevProduct,
    [name]: value,
  }));
};

  const handleAddProduct = () => {
    fetch(`${ process.env.REACT_APP_API_URL}/products`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: localStorage.getItem('token')
      },
      body: JSON.stringify(newProduct),
    })
      .then((response) => response.json())
      .then((data) => {
        setProducts([...products, data]);
        setShowAddModal(false);
        setNewProduct({
          name: '',
          description: '',
          price: 0,
          isAvailable: true,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const handleUpdateProduct = () => {
  fetch(`${ process.env.REACT_APP_API_URL}/products/${editProduct._id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: localStorage.getItem('token')
    },
    body: JSON.stringify(editProduct),
  })
    .then((response) => response.json())
    .then((data) => {
      // find the updated product in the products array and update its data
      const updatedProducts = products.map((product) => {
        if (product._id === editProduct._id) {
          return { ...editProduct };
        }
        return product;
      });
      setProducts(updatedProducts);
      setShowEditModal(false);
      setEditProduct(null);
    })
    .catch((error) => {
      console.log(error);
    });
};


  useEffect(() => {
    fetch(`${ process.env.REACT_APP_API_URL}/products/all`, {
    	method: "GET",
    })
      .then(response => response.json())
      .then(data => {
        setProducts(data);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const toggleProductAvailability = (productId, isAvailable) => {
  fetch(`${ process.env.REACT_APP_API_URL}/products/${productId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: localStorage.getItem('token')
    },
    body: JSON.stringify({ isAvailable }),
  })
    .then((response) => response.json())
    .then((data) => {
      // find the updated product in the products array and update its availability
      const updatedProducts = products.map((product) => {
        if (product._id === productId) {
          return { ...product, isAvailable };
        }
        return product;
      });
      setProducts(updatedProducts);
    })
    .catch((error) => {
      console.log(error);
    });
};


return (
    <>
      <Container className="my-4">
        <h1>Welcome to the Admin Dashboard!</h1>
        <Button variant="primary my-2" onClick={() => setShowAddModal(true)}>
          Add Product
        </Button>
        <Table striped bordered hover>
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Description</th>
      <th>Price</th>
      <th>Available</th>
      <th>Action</th> {/* new column */}
    </tr>
  </thead>
  <tbody>
    {products.map((product) => (
      <tr key={product._id}>
        <td>{product._id}</td>
        <td>{product.name}</td>
        <td>{product.description}</td>
        <td>{product.price}</td>
        <td>{product.isAvailable ? 'Yes' : 'No'}</td>
        <td>
          <Button variant="primary" onClick={() => handleOpenEditModal(product)}>
            Edit
          </Button>
        </td>
        <td>
          <Button
            variant={product.isAvailable ? 'danger' : 'success'}
            onClick={() => toggleProductAvailability(product._id, !product.isAvailable)}
          >
            {product.isAvailable ? 'Deactivate' : 'Reactivate'}
          </Button>
        </td>
      </tr>
    ))}
  </tbody>
</Table>
        <Modal show={showAddModal} onHide={() => setShowAddModal(false)}>
          <Modal.Header closeButton>
            <Modal.Title>Add Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="formProductName">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  value={newProduct.name}
                  onChange={handleInputChange}
                />
              </Form.Group>
              <Form.Group controlId="formProductDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  as="textarea"
                  name="description"
                  value={newProduct.description}
                  onChange={handleInputChange}
                />
              </Form.Group>
              <Form.Group controlId="formProductPrice">
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="number"
                  name="price"
                  value={newProduct.price}
                  onChange={handleInputChange}
                />
              </Form.Group>
              <Form.Group controlId="formProductAvailability">
                <Form.Check
                  type="checkbox"
                  label="Available"
                  name="isAvailable"
                  checked={newProduct.isAvailable}
                  onChange={() =>
                    setNewProduct((prevProduct) => ({
                      ...prevProduct,
                      isAvailable: !prevProduct.isAvailable,
                    }))
                  }
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => setShowAddModal(false)}>
              Cancel
            </Button>
            <Button variant="primary" onClick={handleAddProduct}>
              Add
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal show={showEditModal} onHide={() => setShowEditModal(false)}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="formProductName">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  value={editProduct ? editProduct.name : ''}
                  onChange={(e) =>
                    setEditProduct((prevProduct) => ({
                      ...prevProduct,
                      name: e.target.value,
                    }))
                  }
                />
              </Form.Group>
              <Form.Group controlId="formProductDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  as="textarea"
                  name="description"
                  value={editProduct ? editProduct.description : ''}
                  onChange={(e) =>
                    setEditProduct((prevProduct) => ({
                      ...prevProduct,
                      description: e.target.value,
                    }))
                  }
                />
              </Form.Group>
              <Form.Group controlId="formProductPrice">
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="number"
                  name="price"
                  value={editProduct ? editProduct.price : ''}
                  onChange={(e) =>
                    setEditProduct((prevProduct) => ({
                      ...prevProduct,
                      price: e.target.value,
                    }))
                  }
                />
              </Form.Group>
              <Form.Group controlId="formProductAvailability">
                <Form.Check
                  type="checkbox"
                  label="Available"
                  name="isAvailable"
                  checked={editProduct ? editProduct.isAvailable : false}
                  onChange={() =>
                    setEditProduct((prevProduct) => ({
                      ...prevProduct,
                      isAvailable: !prevProduct.isAvailable,
                    }))
                  }
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
              <Button variant ="secondary" onClick={() => setShowEditModal(false)}>Cancel
          </Button>
          <Button variant="primary" onClick={handleUpdateProduct}>Update
          </Button>
          </Modal.Footer>
          </Modal>
      </Container>
    </>
  );
};

export default AdminDashboard;